using NUnit.Framework;
using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;


namespace SeleniumGridNETs.TestItems
{
    [TestFixture]
    public class SeleniumGridChromeExample
    {

        [Parallelizable(ParallelScope.Self)]
        [Test]
        public void NavigateToGoogleHomePage()
        {
            RemoteWebDriver driver;

            var browserContext = TestContext.Parameters.Get("Browser");

            Console.WriteLine("The BROWSER is {0}",browserContext);
            //CHROME and IE            
            ChromeOptions Options = new ChromeOptions();
            //InternetExplorerOptions Options = new InternetExplorerOptions();
            //Options.PlatformName = "linux";
            Options.AddAdditionalCapability("platform", "LINUX", true); // Supported values: "VISTA" (Windows 7), "WIN8" (Windows 8), "WIN8_1" (windows 8.1), "WIN10" (Windows 10), "LINUX" (Linux)
            Options.AddAdditionalCapability("version", "90.0.4430.85", true); // you can specify version=latest or the version number like version="90". For IE you must always specify the version number.
            //Options.AddAdditionalCapability("video", "True", true);


            driver = new RemoteWebDriver(
              new Uri("http://192.168.1.76:4444/wd/hub"), Options.ToCapabilities(), TimeSpan.FromSeconds(600));// NOTE: connection timeout of 600 seconds or more required for time to launch grid nodes if non are available.
            try
            {
                driver.Manage().Window.Maximize(); // WINDOWS, DO NOT WORK FOR LINUX/firefox. If Linux/firefox set window size, max 1920x1080, like driver.Manage().Window.Size = new Size(1920, 1080);
                //driver.Manage().Window.Size = new Size(1920, 1080); // LINUX/firefox			 
                driver.Navigate().GoToUrl("https://www.google.com/ncr");
                IWebElement query = driver.FindElement(By.Name("q"));
                query.SendKeys("webdriver");
                query.Submit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                //Console.WriteLine("Video: " + VIDEO_URL + driver.SessionId);

                driver.Quit();
            }
        }
        [Parallelizable(ParallelScope.Self)]
        [Test]
        public void NavigateToGoogleHomePageSecond()
        {
            RemoteWebDriver driver;


            //CHROME and IE            
            ChromeOptions Options = new ChromeOptions();
            //InternetExplorerOptions Options = new InternetExplorerOptions();
            //Options.PlatformName = "linux";
            Options.AddAdditionalCapability("platform", "LINUX", true); // Supported values: "VISTA" (Windows 7), "WIN8" (Windows 8), "WIN8_1" (windows 8.1), "WIN10" (Windows 10), "LINUX" (Linux)
            Options.AddAdditionalCapability("version", "90.0.4430.85", true); // you can specify version=latest or the version number like version="90". For IE you must always specify the version number.
            //Options.AddAdditionalCapability("video", "True", true);


            driver = new RemoteWebDriver(
              new Uri("http://192.168.1.76:4444/wd/hub"), Options.ToCapabilities(), TimeSpan.FromSeconds(600));// NOTE: connection timeout of 600 seconds or more required for time to launch grid nodes if non are available.
            try
            {
                driver.Manage().Window.Maximize(); // WINDOWS, DO NOT WORK FOR LINUX/firefox. If Linux/firefox set window size, max 1920x1080, like driver.Manage().Window.Size = new Size(1920, 1080);
                //driver.Manage().Window.Size = new Size(1920, 1080); // LINUX/firefox			 
                driver.Navigate().GoToUrl("https://www.google.com/ncr");
                IWebElement query = driver.FindElement(By.Name("q"));
                query.SendKeys("webdriver 2");
                query.Submit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                //Console.WriteLine("Video: " + VIDEO_URL + driver.SessionId);

                driver.Quit();
            }
        }
    }
}
